const {ipcRenderer} = require('electron')

function sendMessage(channel, msg) {
    ipcRenderer.send(channel, msg);
}

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}

defer(
(function () {
    addons.register({
        init: function(events) {
            events.on('onGetPlayer', this.onGetPlayer.bind(this));
            events.on('onGetStats', this.onGetStats.bind(this));
            events.on('onShowCharacterSelect', this.onShowCharacterSelect.bind(this));

            sendMessage('discord-integration-menuupdate', 'On Login Page');
        },
        onGetPlayer: function(player) {
            const playerInfo = {
                name: player.name,
                level: playerStats.level,
                class: player.class,
                zone: player.zoneName
            };

            sendMessage('discord-integration-playerupdate', playerInfo);
        },
        onGetStats: function(stats) {
            playerStats = stats;
        },
        onShowCharacterSelect: function(obj) {
            sendMessage('discord-integration-menuupdate', 'Selecting a Character');
        }
    });
}));