// Modules to control application life and create native browser window
const { app, globalShortcut, BrowserWindow } = require('electron');
const DataStore = require('./dataStore.js');
const path = require('path');
require('./discord/discordIntegration.js');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let changelogWindow;

const appVersion = app.getVersion();

const dataStore = new DataStore({
  configName: 'user-preferences',
  defaults: {
    windowBounds: { width: 1280, height: 720 },
    isWindowMaximized: false,
    lastSeenChangelog: ''
  }
});

function getIconPath() {
  // Set the  base path for the icon, without the extension
  let iconPath = path.join(__dirname, 'assets/icons/icon');

  // Add the icon extension to the path based on the OS
  // macOS (darwin) will probably not work but it's here just for completeness' sake
  if (process.platform === 'win32') {
    iconPath = `${iconPath}.ico`;
  } else if (process.platform === 'linux') {
    iconPath = `${iconPath}.png`;
  } else if (process.platform === 'darwin') {
    //iconPath = `${iconPath}.icns`
    iconPath = `${iconPath}.png`
  }

  return iconPath;
}

function createMainWindow() {
  const { width, height } = dataStore.get('windowBounds');
  const isWindowMaximized = dataStore.get('isWindowMaximized');
  const iconPath = getIconPath();

  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: width,
    height: height,
    icon: iconPath,
    title: `Isleward (Client v${appVersion})`,
    backgroundColor: "#2d2136",
    webPreferences: {
      nodeIntegration: true,
      webviewTag: true
    }
  });

  if (isWindowMaximized) {
    mainWindow.maximize();
  }

  // and load the index.html of the app.
  mainWindow.loadFile('index.html');

  // Send the userData path to the renderer process once webcontents are loaded
  mainWindow.webContents.on('did-finish-load', function () {
    mainWindow.webContents.send('userDataMessage', app.getPath('userData'));
  });

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Close the changelog window if it's open
    if (changelogWindow) {
      changelogWindow.close();
    }

    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  mainWindow.on('resize', function () {
    let { width, height } = mainWindow.getBounds();
    dataStore.set('windowBounds', { width, height });
  });

  mainWindow.on('maximize', function () {
    dataStore.set('isWindowMaximized', true);
  });

  mainWindow.on('unmaximize', function () {
    dataStore.set('isWindowMaximized', false);
  });
}

function createChangelogWindow() {
  const lastSeenChangelog = dataStore.get('lastSeenChangelog');
  const iconPath = getIconPath();

  if (lastSeenChangelog == appVersion) {
    return;
  }

  changelogWindow = new BrowserWindow({
    width: 800,
    height: 600,
    icon: iconPath,
    title: "Isleward - Changelog",
    skipTaskbar: false
  });

  changelogWindow.loadFile('changelog.html');

  // Emitted when the window is closed.
  changelogWindow.on('closed', function () {
    dataStore.set('lastSeenChangelog', appVersion);
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    changelogWindow = null;
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function () {
  createMainWindow();
  createChangelogWindow();

  // Register Ctrl+Shift+I as a shortcut to open webview's Dev Tools through a message sent via ipc
  globalShortcut.register('CommandOrControl+Shift+I', function () {
    mainWindow.webContents.send('openDevTools', null);
  });
});

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// Disable top menu bar for all windows
app.on('browser-window-created', function (e, window) {
  window.setMenu(null);
});