const fs = require('fs');
const glob = require('glob');
const path = require('path');
const ipcRenderer = require('electron').ipcRenderer;

window.onresize = doLayout;

onload = function() {
  // Receive the userData path from the main process
  let userDataPath = '';
  ipcRenderer.on('userDataMessage', function(event, message) {
    userDataPath = message;
  });

  let webview = document.querySelector('webview');
  doLayout();

  // Test for the presence of the experimental <webview> zoom and find APIs.
  if (typeof(webview.setZoom) == 'function' &&
      typeof(webview.find) == 'function') {
  }

  webview.addEventListener('dom-ready', () => {
    // Receive a message from the main process telling the renderer process to open webview's devtools
    ipcRenderer.on('openDevTools', function(event, message) {
      webview.openDevTools();
    });

    // Load all js files (addons) inside the userData/addons folder
    let addonsDirPath = `${userDataPath}/addons`;
    glob.sync(`${addonsDirPath}/*.js`).forEach(function(file) {
      let addonPath = path.resolve(file);
      let addonScript = fs.readFileSync(addonPath, 'utf8');
      webview.executeJavaScript(addonScript);
    });

    // This fixes the chat window focus bug
    window.focus();
  });
};

function getControlsHeight() {
  let controls = document.querySelector('#controls');
  if (controls) {
    return controls.offsetHeight;
  } else {
    return 0;
  }
}

function doLayout() {
  let webview = document.querySelector('webview');
  let windowWidth = document.documentElement.clientWidth;
  let windowHeight = document.documentElement.clientHeight;

  let controlsHeight = getControlsHeight();

  let webviewWidth = windowWidth;
  let webviewHeight = windowHeight - controlsHeight;

  webview.style.width = webviewWidth + 'px';
  webview.style.height = webviewHeight + 'px';
}

