# Isleward Standalone Client

This is the standalone Electron client for [Isleward](https://gitlab.com/Isleward/isleward).

## Download

Downloads are currently provided as zip archives only and can be found on [Bintray](https://dl.bintray.com/vildravn/Isleward-Client/).

## Changelog

Changelog can be found [here](CHANGELOG.md)

## FAQ

### What do I do if I find a bug?

Please report any issues related to the client itself [here](https://gitlab.com/Isleward/isleward-webview-client/issues).  
For ingame issues, go to the [Isleward repository](https://gitlab.com/Isleward/isleward/issues).

### How do I load addons?

Addons are loaded automatically if placed into the right directory.
Simply place the js files into the following directory (create it if it doesn't exist):

* Windows: `%APPDATA%\isleward-client\addons`
* Linux: `~/.config/isleward-client/addons`
* macOS: `~/Library/Application Support/isleward-client/addons`

### How do I open the Dev Tools/console?

* Windows/Linux: `Control + Shift + I`
* macOS: `Cmd + Shift + I`
